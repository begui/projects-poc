# quarkus-pico-cli-mybatis-example

Create an uber jar. This jar will contiain all the commands you can process

    ./mvnw clean package -Dquarkus.package.type=uber-jar


Example of the following commands

    java -jar target/quarkus-pico-cli-mybatis-1.0.0-SNAPSHOT-runner.jar userAdd -n "Beej"
    java -jar target/quarkus-pico-cli-mybatis-1.0.0-SNAPSHOT-runner.jar userRemove -n 1 
    java -jar target/quarkus-pico-cli-mybatis-1.0.0-SNAPSHOT-runner.jar userList 
    java -jar target/quarkus-pico-cli-mybatis-1.0.0-SNAPSHOT-runner.jar price1
    java -jar target/quarkus-pico-cli-mybatis-1.0.0-SNAPSHOT-runner.jar price2 

You can add all these commands into a shell script and have a linux cron job execute at whatever time

Some limitations, 

This will make a connection to the DB per run, if you are running 3 jobs at the same time, there will be three db connections. Currently there is no way to 'turn off' the database connection at runtime. ( only build time )

This work can still be done outside of quarkus, and you may have more flexibility ( with the database connection on startup ). 



## References 

[Pico Cli](https://quarkus.io/guides/picocli)

[Mybatis Documentation](https://quarkiverse.github.io/quarkiverse-docs/quarkus-mybatis/dev/index.html)

