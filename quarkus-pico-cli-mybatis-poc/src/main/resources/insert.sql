DROP TABLE IF EXISTS USERS;

CREATE TABLE USERS (
    id SERIAL PRIMARY KEY,
    name varchar(80) not null
);

INSERT INTO USERS (id, name) values(1, 'User1');
INSERT INTO USERS (id, name) values(2, 'User2');
INSERT INTO USERS (id, name) values(3, 'User3');