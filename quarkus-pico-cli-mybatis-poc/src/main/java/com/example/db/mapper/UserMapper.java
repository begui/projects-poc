package com.example.db.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.example.db.model.User;

@Mapper
public interface UserMapper {

    @Insert("INSERT INTO USERS (name) VALUES (#{name})")
    Integer createUser( @Param("name") String name);

    @Delete("DELETE FROM USERS WHERE id = #{id}")
    Integer removeUser(Integer id);

    @Select("SELECT * FROM USERS WHERE id = #{id}")
    User getUser(Integer id);

    @Select("SELECT * FROM USERS")
    List<User> getUsers();

}
