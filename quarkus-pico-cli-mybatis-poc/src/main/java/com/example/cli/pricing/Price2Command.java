package com.example.cli.pricing;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import picocli.CommandLine.Command;

@Command(name = "price2", description = {"Price 2 command"},  mixinStandardHelpOptions = true)
public class Price2Command implements Runnable{
    @Inject
    Logger log;

    @Override
    public void run() {
        log.info("Price 2 Job Ran");
    }
    
}
