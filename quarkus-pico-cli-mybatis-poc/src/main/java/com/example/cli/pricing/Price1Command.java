package com.example.cli.pricing;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import picocli.CommandLine.Command;

@Command(name = "price1", description = {"Price 1 command"},  mixinStandardHelpOptions = true)
public class Price1Command implements Runnable {

    @Inject
    Logger log;

    @Override
    public void run() {
      log.info("Price 1 job Ran");
        
    }
    
}
