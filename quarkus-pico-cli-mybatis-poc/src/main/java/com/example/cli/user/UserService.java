package com.example.cli.user;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.example.db.mapper.UserMapper;

@ApplicationScoped
public class UserService {
    @Inject
    Logger log;

    @Inject
    UserMapper userMapper;

    public void addUser(String username) {

        userMapper.createUser(username);
    }

    public void deleteUser(Integer id) {

        userMapper.removeUser(id);
    }

    public void listUsers() {

        var list = userMapper.getUsers();
        for (var l : list) {
            log.info(l.toString());
        }

    }
}
