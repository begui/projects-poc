package com.example.cli.user;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import picocli.CommandLine.Command;

@Command(name = "userList", description = { "List of user commands" }, mixinStandardHelpOptions = true)
public class UserListCommand implements Runnable {

	@Inject
	// @LoggerName("user") // or you can inject and have it go to the user logger
	Logger log;

	@Inject
	UserService userService;

	@Override
	public void run() {
		log.info("Running userList");
		userService.listUsers();
	}

}
