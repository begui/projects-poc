package com.example.cli.user;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.jboss.logging.Logger;

import com.example.db.mapper.UserMapper;

import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "userAdd", description = { "Add user commands" }, mixinStandardHelpOptions = true)
public class UserAddCommand implements Runnable {

	@Inject
	// @LoggerName("user") // or you can inject and have it go to the user logger
	Logger log;

	@Inject
	UserService userService;

	@CommandLine.Option(names = "-n", required = true, description = "mandatory name")
	String name;

	@Override
	public void run() {
		log.infof("Running userAdd %s", name);
		userService.addUser(name);
	}

}