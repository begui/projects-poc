package com.example.cli.user;

import javax.inject.Inject;

import org.jboss.logging.Logger;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "userRemove", description = { "Remove user commands" }, mixinStandardHelpOptions = true)
public class UserRemoveCommand implements Runnable {

	@Inject
	// @LoggerName("user") // or you can inject and have it go to the user logger
	Logger log;

	@Inject
	UserService userService;

	@Option(names = "-n", required = true, description = "mandatory id")
	int id;

	@Override
	public void run() {
		log.infof("Running user remove %d", id);
		userService.deleteUser(id);
	}

}
