package com.example.cli;

import com.example.cli.pricing.Price1Command;
import com.example.cli.pricing.Price2Command;
import com.example.cli.user.UserAddCommand;
import com.example.cli.user.UserListCommand;
import com.example.cli.user.UserRemoveCommand;

import io.quarkus.picocli.runtime.annotations.TopCommand;
import picocli.CommandLine;

@TopCommand
@CommandLine.Command(mixinStandardHelpOptions = true, subcommands = { //
		UserAddCommand.class, //
		UserRemoveCommand.class, //
		UserListCommand.class, //
		Price1Command.class,//
		Price2Command.class
//
})
public class ExampleCliMain {

}
