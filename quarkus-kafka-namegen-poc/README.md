# quarkus-kafka-namegen-poc

## About

This POC generates (random) names in one component. These names are written in a Kafka
topic ( names ). A second component reads from the names Kafka topic and applies some
magic conversion to the name (adding an honorific). The result is sent to an in-memory
stream consumed by a JAX-RS resource. The data is sent to a browser using
server-sent events (https://www.w3.org/TR/eventsource/) and displayed in the browser.

Navigate to http://localhost:8080