package com.begui.frb.resource;

import javax.validation.constraints.NotBlank;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.begui.frb.db.Ach;
import com.begui.frb.db.Wire;

import org.jboss.resteasy.reactive.RestQuery;

@Path("/frb")
public class FrbResource {

	@GET
	@Path("/ach")
	public Response findAchRouting(@RestQuery @NotBlank String routingNumber) {
		var rtn = Ach.findByRoutingNumber(routingNumber);
		if (rtn == null) {
			return Response.noContent().build();
		}
		return Response.ok(rtn).build();
	}

	@GET
	@Path("/wire")
	public Response findWireRouting(@RestQuery @NotBlank String routingNumber) {
		var rtn = Wire.findByRoutingNumber(routingNumber);
		if (rtn == null) {
			return Response.noContent().build();
		}
		return Response.ok(rtn).build();
	}
}