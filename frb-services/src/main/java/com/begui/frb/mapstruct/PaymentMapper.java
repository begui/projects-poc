package com.begui.frb.mapstruct;

import com.begui.frb.AchRecord;
import com.begui.frb.WireRecord;
import com.begui.frb.db.Ach;
import com.begui.frb.db.Wire;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

public interface PaymentMapper<PaymentEntity, PaymentRecord> {

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createDate", ignore = true)
	@Mapping(target = "updateDate", ignore = true)
	public PaymentEntity toPaymentType(PaymentRecord record);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "createDate", ignore = true)
	@Mapping(target = "updateDate", expression = "java(new java.util.Date())")
	public void updatePaymentType(PaymentRecord record, @MappingTarget PaymentEntity entity);
	
	@Mapper(componentModel = "cdi")
    public interface WireMapper extends PaymentMapper<Wire, WireRecord> {

    }
	@Mapper(componentModel = "cdi")
    public interface AchMapper extends PaymentMapper<Ach, AchRecord> {

    }

	
	


}
