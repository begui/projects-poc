package com.begui.frb;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.begui.frb.db.Wire;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class WireRoute extends GenericRoute<Wire, WireRecord > {
	
	WireRoute() {
		// Needed for now
		super(null, Wire.class, WireRecord.class);
	}

	@Inject
	public WireRoute(@ConfigProperty(name = "frb.wire.directory") String path) {
		super(path, Wire.class,WireRecord.class);
	}

}
