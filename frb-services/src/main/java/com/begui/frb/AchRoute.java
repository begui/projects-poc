package com.begui.frb;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.begui.frb.db.Ach;

import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class AchRoute extends GenericRoute<Ach, AchRecord > {
	
	AchRoute() {
		// Needed for now
		super(null, Ach.class, AchRecord.class);
	}

	@Inject
	public AchRoute(@ConfigProperty(name = "frb.ach.directory") String path) {
		super(path, Ach.class,AchRecord.class);
	}

}
