package com.begui.frb;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class AchRecord {

	@DataField(length = 9, pos = 1)
	public String routingNumber;

	@DataField(length = 1, pos = 10)
	public String officeCode;

	@DataField(length = 9, pos = 11)
	public String servicingFrbNumber;

	@DataField(length = 1, pos = 20)
	public Integer recordTypeCode;

	@DataField(length = 6, pos = 21, pattern = "MMddyy")
	public Date changeDate;

	@DataField(length = 9, pos = 27)
	public String newRoutingNumber;

	@DataField(length = 36, pos = 36)
	public String customerName;

	@DataField(length = 36, pos = 72)
	public String address;

	@DataField(length = 20, pos = 108)
	public String city;

	@DataField(length = 2, pos = 128)
	public String stateCode;

	@DataField(length = 5, pos = 130)
	public String zipCode;

	@DataField(length = 4, pos = 135)
	public String zipCodeExtension;

	@DataField(length = 3, pos = 139)
	public String telephoneAreaCode;

	@DataField(length = 3, pos = 142)
	public String telephonePrefixNumber;

	@DataField(length = 4, pos = 145)
	public String telephoneSufixNumber;

	@DataField(length = 1, pos = 149)
	public String institutionStatusCode;

	@DataField(length = 1, pos = 150)
	public String dataViewCode;

	// Needed for apache camel OGNL
	public String getRoutingNumber() {
		return routingNumber; 
	}

}
