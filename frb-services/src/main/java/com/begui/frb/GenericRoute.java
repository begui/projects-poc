package com.begui.frb;

import java.util.ArrayList;

import javax.inject.Inject;

import com.begui.frb.mapstruct.PaymentMapper;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.fixed.BindyFixedLengthDataFormat;


public abstract class GenericRoute<TEntity, TRecord > extends RouteBuilder {
	private static final String HEADER_RECORD = "record";

	private final String fromPath;
	private final Class<TEntity> tEntityClazz;
	private final Class<TRecord> tRecordClazz;
	GenericRoute(String path, Class<TEntity> tPaymentClazz, Class<TRecord> tRecordClazz){
		this.fromPath = path;
		this.tEntityClazz = tPaymentClazz;
		this.tRecordClazz = tRecordClazz;
	}

	@Inject
	public PaymentMapper<TEntity, TRecord> mapper;


	@Override
	public void configure() throws Exception {

		var fromFileRoute = String.format("file:%s/?autoCreate=true&move=archive&moveFailed=error", fromPath);
		from(fromFileRoute) //
				.log(fromFileRoute)
				.unmarshal(new BindyFixedLengthDataFormat(tRecordClazz)) //
				.split(body())//
				.parallelProcessing()
				.process(new Processor() {
					@Override
					public void process(Exchange exchange) throws Exception {
						TRecord record = exchange.getMessage().getBody(tRecordClazz);
						exchange.getMessage().setHeader(HEADER_RECORD, record);
					}
				})
				.log(LoggingLevel.INFO, "Routing Number: '${body.routingNumber}' ")
				.enrich().simple("jpa://" + tEntityClazz.getName() +"?query=select p from "+ tEntityClazz.getName() +" p where p.routingNumber = '${body.routingNumber}'")
				.choice()
					.when(simple("${body.size} == 0"))
						.log(LoggingLevel.DEBUG, "Inserting ${header.record.routingNumber}")
						.process(new Processor() {
							@Override
							public void process(Exchange exchange) throws Exception {
								TRecord  record = exchange.getMessage().getHeader(HEADER_RECORD, tRecordClazz);
								if(record != null) {
									var entity = mapper.toPaymentType(record);
									exchange.getIn().setBody(entity);
								}
							}
						 })
					.otherwise()
						.log(LoggingLevel.DEBUG, "Updating ${header.record.routingNumber}")
						.process(new Processor() {
							@Override
							public void process(Exchange exchange) throws Exception {
								TRecord record = exchange.getMessage().getHeader(HEADER_RECORD, tRecordClazz);
								@SuppressWarnings("unchecked")
								ArrayList<TEntity> list = exchange.getIn().getBody(ArrayList.class);
								//TODO: what if there is more than one element in the file..
								TEntity toUpdate = list.get(0);
								mapper.updatePaymentType(record, toUpdate);
								exchange.getIn().setBody(toUpdate);
							}
						 })
				.endChoice()
			.end()
			.to("jpa://" + tEntityClazz.getName() + "?usePersist=true&useExecuteUpdate=true")
			.end()
			.log("Completed");
	}

}
