package com.begui.frb;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class WireRecord {

	@DataField(length = 9, pos = 1)
	public String routingNumber;
	@DataField(length = 18, pos = 10)
	public String telegraphicName;
	@DataField(length = 2, pos = 64)
	public String stateCode;
	@DataField(length = 25, pos = 66)
	public String city;
	@DataField(length = 1, pos = 91)
	public String fundsTransferStatus;
	@DataField(length = 1, pos = 92)
	public String fundsSettlementStatus;
	@DataField(length = 1, pos = 93)
	public String bookEntryTransferStatus;
	@DataField(length = 8, pos = 94, pattern = "yyyyMMdd") // YYYYMMDD
	public Date revisionDate;

  // Needed for apache camel OGNL
  public String getRoutingNumber() {
    return routingNumber;
  }

}
