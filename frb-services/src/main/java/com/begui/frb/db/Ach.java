package com.begui.frb.db;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
@Table(indexes = {
        @Index(name = "ach_routing_routing_number_index", columnList = "routingNumber"),
})
@NamedQuery(name = "getByRoutingNumber", query = "from Ach ach where ach.routingNumber = $1")
public class Ach extends PanacheEntity {

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	public Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	public Date updateDate;

	@Column(name = "routingNumber", length = 9, nullable = false)
	public String routingNumber;

	@Column(length = 1, columnDefinition = "char")
	public String officeCode;

	@Column(length = 9)
	public String servicingFrbNumber;

	@Column
	public Integer recordTypeCode;

	@Column
	public Date changeDate; 

	@Column(length = 9, nullable = false)
	public String newRoutingNumber;

	@Column(length = 36)
	public String customerName;

	@Column(length = 36)
	public String address;

	@Column(length = 20)
	public String city;

	@Column(length = 2)
	public String stateCode;

	@Column(length = 5)
	public String zipCode;

	@Column(length = 4)
	public String zipCodeExtension;

	@Column(length = 3)
	public String telephoneAreaCode;

	@Column(length = 3)
	public String telephonePrefixNumber;

	@Column(length = 4)
	public String telephoneSufixNumber;

	@Column(length = 1)
	public Integer institutionStatusCode;

	@Column(length = 1)
	public Integer dataViewCode;

	public static Ach findByRoutingNumber(String routingNumber) {
		return Ach.find("routingNumber", routingNumber).firstResult();
	}

}

