package com.begui.frb.db;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity
public class Wire extends PanacheEntity {
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date")
	public Date createDate;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	public Date updateDate;
	
	@Column(name = "routingNumber", length = 9, nullable = false)
	public String routingNumber;
	@Column(length = 18)
	public String telegraphicName;
	@Column(length = 2)
	public String stateCode;
	@Column(length = 25)
	public String city;
	@Column(length = 1)
	public String fundsTransferStatus;
	@Column(length = 1)
	public String fundsSettlementStatus;
	@Column(length = 1)
	public String bookEntryTransferStatus;
	@Column(length = 8) // YYYMMDD
	public Date revisionDate;

	public static Wire findByRoutingNumber(String routingNumber) {
		return Wire.find("routingNumber", routingNumber).firstResult();
	}
	
	
}