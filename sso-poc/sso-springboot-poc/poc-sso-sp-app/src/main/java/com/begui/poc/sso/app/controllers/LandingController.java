package com.begui.poc.sso.app.controllers;

import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LandingController {

	private static final Logger log = LoggerFactory.getLogger(LandingController.class);

	@GetMapping(path = "/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "pages/index";
	}

	@GetMapping("/spsamlapp")
	public ModelAndView toSamlPage(ModelMap model) {
		model.addAttribute("attribute", "att1");
		// TODO; is there a way to fetch this from keycloak
		return new ModelAndView("redirect:http://192.168.1.77:8082/poc-sso-sp-saml/landing", model);
	}

	@GetMapping("/spoauth2app")
	public ModelAndView toOauth2Page(ModelMap model) {
		model.addAttribute("attribute", "att2");
		// TODO: is there a way to fetch this from keycloak
		return new ModelAndView("redirect:http://192.168.1.77:8083/poc-sso-sp-oauth2/landing", model);
	}

	@GetMapping("/spkeyadpapp")
	public ModelAndView toKeyCloakAdptPage(ModelMap model) {
		model.addAttribute("attribute", "att3");
		// TODO: is there a way to fetch this from keycloak
		return new ModelAndView("redirect:http://192.168.1.77:8084/poc-sso-sp-keyadp/landing", model);
	}

	@RequestMapping("/landing")
	public String landing(Model model) {

		final SecurityContext securityContext = SecurityContextHolder.getContext();
		final KeycloakAuthenticationToken auth = (KeycloakAuthenticationToken) securityContext.getAuthentication();
		log.debug("Current authentication instance from security context: "
				+ ((auth != null) ? this.getClass().getSimpleName() : "null"));
		if (auth != null) {

			log.info("Context is {}", securityContext);
			log.info("Authentication is {}", securityContext.getAuthentication());
			log.info("Principal is {}", securityContext.getAuthentication().getPrincipal());
			log.info("Details is {}", securityContext.getAuthentication().getDetails());

			final String userid = auth.getAccount().getKeycloakSecurityContext().getIdToken().getSubject();
			final String user = auth.getAccount().getKeycloakSecurityContext().getIdToken().getPreferredUsername();
			model.addAttribute("userid", userid);
			model.addAttribute("username", user);
		}
		return "pages/landing";
	}

}
