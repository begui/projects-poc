package com.begui.poc.sso.app.config;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.adapters.springsecurity.KeycloakConfiguration;
import org.keycloak.adapters.springsecurity.KeycloakSecurityComponents;
import org.keycloak.adapters.springsecurity.authentication.KeycloakAuthenticationProvider;
import org.keycloak.adapters.springsecurity.client.KeycloakClientRequestFactory;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.keycloak.adapters.springsecurity.config.KeycloakWebSecurityConfigurerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.authority.mapping.SimpleAuthorityMapper;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.session.RegisterSessionAuthenticationStrategy;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@KeycloakConfiguration
@EnableWebSecurity
@ComponentScan(basePackageClasses = KeycloakSecurityComponents.class)
public class WebSecurityConfig extends KeycloakWebSecurityConfigurerAdapter {

	@Autowired
	public KeycloakClientRequestFactory keycloakClientRequestFactory;

	/**
	 * Registers the KeycloakAuthenticationProvider with the authentication manager.
	 */
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

		SimpleAuthorityMapper grantedAuthorityMapper = new SimpleAuthorityMapper();
		grantedAuthorityMapper.setPrefix("ROLE_");

		KeycloakAuthenticationProvider keycloakAuthenticationProvider = keycloakAuthenticationProvider();
		keycloakAuthenticationProvider.setGrantedAuthoritiesMapper(grantedAuthorityMapper);
		auth.authenticationProvider(keycloakAuthenticationProvider);

	}

	/**
	 * Provide a session authentication strategy bean which should be of type
	 * RegisterSessionAuthenticationStrategy for public or confidential applications
	 * and NullAuthenticatedSessionStrategy for bearer-only applications.
	 */
	@Bean
	@Override
	protected SessionAuthenticationStrategy sessionAuthenticationStrategy() {
		return new RegisterSessionAuthenticationStrategy(new SessionRegistryImpl());
	}

	/**
	 * Use properties in application.properties instead of keycloak.json
	 */
	@Bean
	@Primary
	public KeycloakConfigResolver keycloakConfigResolver(KeycloakSpringBootProperties properties) {
		return new CustomKeycloakSpringBootConfigResolver(properties);
	}

	/**
	 * Spring Security Adapter.
	 * 
	 * TODO: We may not need this. 2.1.9.
	 */
	@Bean
	public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
		return new ServletListenerRegistrationBean<HttpSessionEventPublisher>(new HttpSessionEventPublisher());
	}

	/**
	 * Spring Security Adapter.
	 * 
	 * To simplify communication between clients, Keycloak provides an extension of
	 * Spring’s RestTemplate that handles bearer token authentication for you. To
	 * enable this feature your security configuration must add the
	 * KeycloakRestTemplate bean. Note that it must be scoped as a prototype to
	 * function correctly.
	 * 
	 * @return
	 */

	@Bean
	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	public KeycloakRestTemplate keycloakRestTemplate() {
		return new KeycloakRestTemplate(keycloakClientRequestFactory);
	}

	/**
	 * Secure appropriate endpoints
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);
		//
		http.authorizeRequests() //
				.antMatchers("/").permitAll() //
				.antMatchers("/css/**").permitAll() //
				.antMatchers("/img/**").permitAll() //
				.antMatchers("/webjars/**").permitAll() //
				.antMatchers("/access").hasRole("SSOUSER").anyRequest().authenticated() //
				.and().logout().logoutSuccessUrl("/");

		// Allow showing pages within a frame
//	    http.headers().frameOptions().sameOrigin();
	}

}
