package com.begui.poc.sso.app.controllers;

import java.security.Principal;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ResourceController {
	
	@GetMapping("/access")
	@PreAuthorize("hasAuthority('SSOUser')")
    public String getResource(Principal model) {
		
        return "Access To Resource";
    }

}
