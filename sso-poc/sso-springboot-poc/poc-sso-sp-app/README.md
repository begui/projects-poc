# poc-sso-sp-app

This application is a Service Provider (SP) using the Keycloak Spring Boot Adapter. This would be the 'home' user application


## KeyCloak Setup

Create a new openid-connect client

    | Parameter            | Value                |
    |----------------------|----------------------|
    | Client ID            | com::begui::sp::app  |
    | Client Protocol      | openid-connect       |

Hit next and keep most of the defaults

    | Parameter                                       | Value                                 |
    |-----------------------------|---------------------------------------|
    | Access Type                 | public 								   |
    | Root URL                    | http://localhost:8081/poc-sso-sp-app  |
    | Valid Redirect URIs         | /*                                    |
    | Admin URL                   | http://localhost:8081/poc-sso-sp-app  |


## Notes

The Admin URL will make callbacks to the Admin URL to do things like backchannel logout.

Another small limitation is limited support for Single-Sign Out. It works without issues if you init servlet logout (HttpServletRequest.logout) from the application itself as the adapter will delete the KEYCLOAK_ADAPTER_STATE cookie. However, back-channel logout initialized from a different application isn’t propagated by Keycloak to applications using cookie store. Hence it’s recommended to use a short value for the access token timeout (for example 1 minute).

Backchannel logout does not currently work when you have application that uses the SAML filter. 


[WellKnown](http://{keycloak_host}:{keycloak_port}/auth/realms/poc-sso-realm%20/.well-known/openid-configuration)


## References

[Spring Boot Keycloak Adapter](https://www.keycloak.org/docs/latest/securing_apps/#_spring_boot_adapter)

[spring-boot-keycloak](https://www.baeldung.com/spring-boot-keycloak)
[spring-boot-keycloak-security](https://www.thomasvitale.com/spring-boot-keycloak-security)
