package com.begui.poc.sso.sp.saml.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.begui.poc.sso.sp.saml.sterotypes.CurrentUser;

@Controller
public class LandingController {

	private static final Logger LOG = LoggerFactory.getLogger(LandingController.class);

	@RequestMapping("/landing")
	public String landing(@CurrentUser User user, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		LOG.debug("Current authentication instance from security context: "
				+ ((auth != null) ? this.getClass().getSimpleName() : "null"));
		if (auth != null) {
			model.addAttribute("username", user.getUsername());
		}
		return "pages/landing";
	}

}
