# poc-sso-sp-saml

This application is a Service Provider (SP).

The Keycloak is the Identity Provider (IDP).

Access the application via http://localhost:8082/poc-sso-sp-saml/

## KeyCloak Setup

Create a new SAML client

    | Parameter            | Value                |
    |----------------------|----------------------|
    | Client ID            | com::begui::sp::saml |
    | Client Protocol      | saml                 |
    | Client SAML Endpoint |                      |

Hit next and keep most of the defaults

    | Parameter                                       | Value                                 |
    |-------------------------------------------------|---------------------------------------|
    | Signature Algorithm                             | RSA_SHA1                              |
    | Name ID Format                                  | username                              |
    | Root URL                                        | http://localhost:8082/poc-sso-sp-saml |
    | Valid Redirect URIs                             | /saml/*                               |
    | Base URL                                        | /                                     |
    | Assertion Consumer Service POST Binding URL     | /saml/SSO                             |
    | Assertion Consumer Service Redirect Binding URL |                                       |
    | Logout Service POST Binding URL                 | /saml/logout                          |
    | Logout Service Redirect Binding URL             |                                       |


Now under SAML Keys lets create a new keystore. For the demo, i keep my samlKeystore.jks located in the $HOME directory

Generate a samlKeystore

    keytool -genkeypair -alias poc-sso-sp-saml -keyalg RSA -keystore samlKeystore.jks -storepass password -noprompt

Import and add the following

    | Parameter      | Value           |
    |----------------|-----------------|
    | Archive Format | JKS             |
    | Key Alias      | poc-sso-sp-saml |
    | Store Password | password        |


Once this is done you can go to the application located at

    http://localhost:8082/poc-sso-sp-saml

## Application setup

All the configuration information is already configured in the applications.properties

    poc.sso.keycloak.auth-server-url=http://192.168.1.91:9990/auth/realms/poc-sso-realm
    poc.sso.keycloak.clientId=com::begui::sp::saml
    poc.sso.keycloak.key.alias=poc-sso-sp-saml
    poc.sso.keycloak.key.password=password
    poc.sso.keycloak.key.path=${user.home}/samlKeystore.jks

## References

[spring-security-saml](https://projects.spring.io/spring-security-saml/)

[spring-security-saml-vdentaris](https://sbs3.vdenotaris.com)

[How to setup a java keystore or SAML](https://support.datameer.com/hc/en-us/articles/205525884-How-to-Setup-a-Java-KeyStore-for-a-SAML-Configuration)
