# poc-sso-sp-oauth

This application is a Service Provider (SP).

The Keycloak is the Identity Provider (IDP).

Access the application via http://localhost:8083/poc-sso-sp-oauth/

## KeyCloak Setup

Create a new SAML client

| Parameter            | Value                |
|----------------------|----------------------|
| Client ID            | com::begui::sp::oauth|
| Client Protocol      |                      |


## Notes

OpenID Connect is an identify layer built on top of OAuth 2

## References
[Spring Security oauth2](https://projects.spring.io/spring-security-oauth/docs/oauth2.html)
[Oauth 2 migration](https://github.com/spring-projects/spring-security/wiki/OAuth-2.0-Migration-Guide)
[Spring OpenId Connect](https://www.baeldung.com/spring-security-openid-connect)
