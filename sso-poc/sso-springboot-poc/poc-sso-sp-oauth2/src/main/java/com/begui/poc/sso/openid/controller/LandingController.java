package com.begui.poc.sso.openid.controller;

import javax.servlet.ServletException;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LandingController {

	@GetMapping(path = "/")
	public String loginPage(Model model, @AuthenticationPrincipal OidcUser oidcUser)
			throws ServletException {
		return "pages/index";
	}

//	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping("/landing")
	public String landing(Model model, @AuthenticationPrincipal OidcUser oidcUser, @AuthenticationPrincipal OAuth2AuthenticationToken authToken) {
		
		model.addAttribute("username",authToken.getPrincipal().getName());
		model.addAttribute("userid", oidcUser.getIdToken().getSubject());
//		model.addAttribute("profile", Collections.singletonMap("details", oidcUser.getAttributes() ));
		return "pages/landing";
	}
	
	 

}
