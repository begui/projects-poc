package com.begui.poc.sso.openid.config;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter implements InitializingBean, DisposableBean {

	@Autowired
	Environment env;

	@Autowired
	private ClientRegistrationRepository clientRegistrationRepository;

	private LogoutSuccessHandler oidcLogoutSuccessHandler() {
		OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler = new OidcClientInitiatedLogoutSuccessHandler(
				this.clientRegistrationRepository);

		final String port = env.getProperty("server.port");
		oidcLogoutSuccessHandler
				.setPostLogoutRedirectUri(URI.create("http://192.168.1.65:" + port + "/poc-sso-sp-oauth2/"));

		return oidcLogoutSuccessHandler;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http //
				.csrf().disable() // Disabling CSRF prevents the use of logout screen (dont do this, logout post with csrf )
				.authorizeRequests(authorizeRequests -> authorizeRequests
						.antMatchers("/", "/css/**", "/error", "/img/**", "/webjars/**").permitAll().anyRequest()
						.authenticated())
				.oauth2Login(oauthLogin -> oauthLogin.permitAll()) //
				.logout(l -> l.logoutSuccessUrl("/").permitAll()) //
				.logout(l -> l.invalidateHttpSession(true)) //
				.logout(l -> l.deleteCookies("JSESSIONID")) //
				.logout(l -> l.logoutSuccessHandler(oidcLogoutSuccessHandler())) //
		;

	}

	@Override
	public void destroy() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}

}
