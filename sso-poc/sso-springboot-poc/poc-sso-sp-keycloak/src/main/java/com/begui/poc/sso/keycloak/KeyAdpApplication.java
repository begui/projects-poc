package com.begui.poc.sso.keycloak;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeyAdpApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeyAdpApplication.class, args);

	}

}
