package com.begui.poc.sso.keycloak.controllers;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LandingController {

	private static final Logger log = LoggerFactory.getLogger(LandingController.class);

	@GetMapping(path = "/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "pages/index";
	}

	@PreAuthorize("hasRole('ROLE_USER')")
	@RequestMapping("/landing")
	public String landing(Model model) {

		final SecurityContext securityContext = SecurityContextHolder.getContext();
		final KeycloakAuthenticationToken auth = (KeycloakAuthenticationToken) securityContext.getAuthentication();
		log.debug("Current authentication instance from security context: "
				+ ((auth != null) ? this.getClass().getSimpleName() : "null"));
		if (auth != null) {

			log.info("Context is {}", securityContext);
			log.info("Authentication is {}", securityContext.getAuthentication());
			log.info("Principal is {}", securityContext.getAuthentication().getPrincipal());
			log.info("Details is {}", securityContext.getAuthentication().getDetails());

			final String userid = auth.getAccount().getKeycloakSecurityContext().getIdToken().getSubject();
			final String user = auth.getAccount().getKeycloakSecurityContext().getIdToken().getPreferredUsername();
			model.addAttribute("userid", userid);
			model.addAttribute("username", user);
		}
		return "pages/landing";
	}

}
