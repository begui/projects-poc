# OAuth2

Oauth 2 is a Framework to enable a person to authorize something. You use Oauth for authetication, it is not a authentication protocol. OAuth2 is for AuthZ only by design without sharing AuthN. AuthN is handled by the Identify Provider ( Example, Keycloak )

There are Oauth2 Extensions to support things not in the standard. Extensions are not uniform from provider to provider. which makes things open to interpretation. When someone claims they support Oauth2, you need to see what extensions are provided.

## OpenId Connect (OCID)

OAuth doesn't specify the identify, we know someone AuthN + AuthZ but not WHO actually did it.

OpenID connect is an OAuth 2 extension for simple identity that defines specific fields for SHARING profile information such as address, phone, email, etc. This is done via an ID token. It does nothing for authorization. For User profile information.

**id_token(signed Json)** - (similar to SAML Assertion)

**OpenID Provier (OP)** - (similar to SAML IdP) Resource Server and Authorization Server.

**RP (Relying Party)** - (Similar to SAML Service Provider)

**Scopes**

  - What am I allowed to do. Scopes are not interchangeable betwen system.
  - Maps to some permission.
  - When the website asks permissions to act on our behalf, it will request scopes and they vary from site to site.
  - Example1: From google, some external website wants to see your age/contacts.
  - Example2: App will update your google calandar.

**Claim**

  - Key/Value pair within the token that gives the client application information.
  - Typically found in JWT token (see below)

## Endpoints

#### Core Endpoints (RFC 6749)

**/authorize** - the end user ( resource owner ) interacts with to grant permission to the resource for the application.  Could return an auth code or access token

**/token** - the application will use to trade an authorization code or refersh token for an access token.

Note: Notice there is no endpoint for revoke, This is defined in RFC 7009

#### Extension Endpoints

RFC 7662 **/introspection**

  - Exapmles a token to describe it's content
  - Useful for opaque tokens
  - Query the Auth server for Token validation
  - Required for any token revocation actions

RFC 7009 **/revoke**

  - Invalidates a token.
  - It's not full proof and may not work the same accross distributed systems.

RFC 7591 **/register**

  - applications use to create new OAuth2 clients (client_id and client_secret) for provisioning new applications or users.

OpenID Connect Core

**/userinfo**

  - Retrieve profile information about the authenticated user.
  - Returns spec-defined set of fields, depending on permissions(scope) requested.

Working Draft

**/.well-known/openid-configuration** - lists all avail. endpoints.

Discovery Documents

    MetaData discovery documents
    TODO: Look up

## About Tokens

Two types of Tokens in core Oauth

- Access Token: Give application accesss to the protected resource.
- Refresh Token: Use to request new access tokens

Third token in OpenID connect

- ID Token: Designed for user profile information. You can copy the content of a id token into the **jsonwebtoken.io** website to see it's contents. (base64 encoded)

Access Token:

- Permission to act as the user.
- passed along with permissions,  We NEVER SHARE the password.
- Tokens are strings and might be opaque (may not have data).
- Represent specific scope and duration of access granted by the resource owner and enforced by the resource server and authorization server.
- Represent scopes and expiration/duration aspect.
- They can have data in it, but not user / password information.
- Uses two type of tokens:
   - Bearer - unguessable string
   - RFC 7519 JWT - assertion, json that contains a siganture,  JWE (Encrypted JWT)

#### RFC 7519 Jason Web Token

The following are common **claims** defined in the specifiction

**iss** - the issuer of the token, an entity we trust, (issuer)
**iat** - (issued at)
**sub**  - the subject (user) of the token,
**aud** - the audience or intended recipient of the token
**exp** - the expiration of the token

Claims are not standard and can be made up. There are some standards in health and for example, financial

[Financial-grade API (FAPI) WG \| OpenID](https://openid.net/wg/fapi/)

## Grant Types

Authorization Code Flow (this is what we will use)

- For backend applications.
- Get back an auth code instead of an access token. We never see the access token
- A little more complex, need an OAuth2 client
- Security, DO NOT IMPLEMENT YOUR OWN. Confirm that your server doesn't accept any arbitary "redirect_uri"
- Resource server validate tokens
- Look up 2017 Google Docs Worm

Client Credential Flow (client_credentials)

- server to server interaction. No user involved (not compatibile with OIDC).
- This is for "private" clients where the interaction is handled by the backed code. - This would not be implemented in a mobile or single page app.
- This will be to be secured ( out of scope )
- Validate the access token via **/introspect** validation rule (RFC 7519)
- Security, DO NOT IMPLEMENT YOUR OWN

Implicit or Hybrid Flow

- for Mobile / Single Page apps, apps we cannot trust ( secrets are not stored in the backend )
- First time that it attaches a user to the process
- For Mobile - AppAuth
- For Single Page - Passport
- Security, DO NOT IMPLEMENT YOUR OWN

Resource Owner Password Flow

- Used as a bridge for Legacy , skip this for now

## Definitions

**Flow or Grant Type** - the exchange where the website sends us to another website and returns.

## Resources

[JSON Web Token (JWT)](https://www.jsonwebtoken.io/)

[Map of OAuth 2.0 Specs - OAuth 2.0 Simplified](https://www.oauth.com/oauth2-servers/map-oauth-2-0-specs/)

