# KeyCloak

## About

Acts as an authorization access managment server and identity provider

Support for

  - Oauth2 with the OpenID Connect protocol
  - Saml 2 protocol
  - Docker Auth - can secure your docker registry and do a docker login defind in keycloak
  - Directory Services (OpenLDap,etc)
  - Built in User managment
  - Social Login(Google, etc)

### Realms

A space for all your applications and users so you can sandbox each of them.

A realm manages a set of users, credentials, roles, and groups. A user belongs to and logs into a realm. Realms are isolated from one another and can only manage and authenticate the users that they control.

Realms Contain the following Concepts

 - Users - keycloak store users
 - Identify Provider - User tore from SAML, OpenID Connect, Social login ( this is where we see their login page )
 - User Federation - User store from LDAP, Kerberos
 - Clients - Service Proviers or Resources (web applications)
 - Roles ( can be client level or global level roles )
 - Themes ( login themes )

### Token Support

- Access Tokens - like Oauth2
- Refersh Tokens - like Oauth2
- Offline Token - Longer time token mainly used for mobile apps
- ID - Token (OIDC)
  - Encapsulates some user information ( User id, User name, etc)
  - Contains set of Claims, set of user information and meta data( payload data ). In JWT it will be the key/value pairs
  - The user info and meta data can be standardized.
  - JWT Tokens are encoded self contained json web token.  (jwt.io)


### Apache Models

- mod_auth_oidc
- mod_auth_mellon (saml)


