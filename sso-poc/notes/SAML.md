# SAML

The Security Assertition Markup Language (SAML) allows single sign-on within a web browser accross a variety of systems. Excange authentication and authorization for sso to applications.

SAML Web SSO Profile - set of xml messages that are exchanged between two parties. These messages can be signed and encrypted with uer information in the form of a SAML Assertion. When you think of SAML Assertions, think signed xml.

Two common forms of communcation between two parties

- POST-Binding
- Redirect Binding

Was designed primarily for SSO( AuthN )


**Identity Provider (IdP)** - organization that provides proof of identity (Google, Facebook, Etc). It issues authentication assertions in conjunction with an SSO profile of SAML. It is something that can hold your credentials you authenticate against.

There are two types of Identify Providers
 * Saml
 * OpenID
 * Social

**Service provider(SP)** - the service the principle is trying to access. He consume the assertions or receives and accptions an authenticaion assertion issued by an identify provider.

**Principle** - the end user

**Identy Broker** - a software layer that authenticates a set of credentials againts an IdP. Once authentication is successful, fetches temporary authentication credentials ( including a token ) from XYZ and returns it to the requestor
