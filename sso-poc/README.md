# sso-poc

## Docker Installation

### Fedora Server

Follow the instructions here


[For Fedora 32](https://computingforgeeks.com/how-to-install-docker-on-fedora/)

Once rebooted

    docker run hello-world

There was an issue when starting docker

[#995](https://github.com/docker/for-linux/issues/955#issuecomment-620932240)

docker: Error Cgroups

    sudo dnf intall -y grubby
    sudo grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
    sudo reboot

### Docker Compose

Docker Compose file has an issue where Keycloak tries to connect before MariaDB can insert its username. Do not use for now.

Instead, run the following

Create sso-network

    docker network create sso-network

**MariaDB**

    docker run -d -p 3306:3306 --name mariadb-container --net sso-network -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=keycloakdb -e MYSQL_USER=keycloak -e MYSQL_PASSWORD=password mariadb:latest

**OpenLdap**

    docker run -d -p 389:389 --name openldap-container --net sso-network -e LDAP_ORGANISATION=Org -e LDAP_DOMAIN=orgdomain.com -e LDAP_ADMIN_PASSWORD=password osixia/openldap:1.3.0

**phpLdapAdmin**

    docker run -d -p 8084:443 --name phpldapadmin-container --net sso-network -e PHPLDAPADMIN_LDAP_HOSTS=172.17.0.1 osixia/phpldapadmin:0.9.0

**Keycloak**

Note: (before starting, verify MariaDB is running with the keycloak user.. or wait about a min)

Note: if keycloak fails to connect. Add the following -e DB_ADDR=172.17.0.1

    docker run -d -p 9990:8080 -p 9991:8443 --name keycloak-container --net sso-network -e DB_VENDOR=mariadb -e DB_DATABASE=keycloakdb -e DB_USER=keycloak -e DB_PASSWORD=password -e KEYCLOAK_USER=keycloak -e KEYCLOAK_PASSWORD=password jboss/keycloak:10.0.2

## Applications

**OpenLdap**

[Gid Number issue](https://stackoverflow.com/questions/19294393/template-value-error-gidnumber-phpldapadmin)

USER: cn=admin,dc=orgdomain,dc=com
PASS: password

There is a 'mock_data.ldif' you can use. Import the data via the phpLdapAdmin tool.

**KeyCloak**

Create a new Realm

    poc-sso-realm

Click on User federation and select ldap

Obtain the openldap-container ip

    docker inspect --format '{{range  .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' openldap-container

Under the required settings, Add the following and leave the rest as defaults

| Parameter       | Value                        |
|-----------------|------------------------------|
| Vendor          | Other                        |
| Connection URL  | ldap://172.18.0.4:389        |
| Users DN        | dc=orgdomain,dc=com          |
| Bind DN         | cn=admin,dc=orgdomain,dc=com |
| Bind Credential | password                     |

Once Saved, Synchronize All Users and verify the users are present in the 'Users' tab.


**sso-springboot-poc**

Setting up poc-sso-sp-app(keycloak application)

Follow the instructions in the [README.md](./sso-springboot-poc/poc-sso-sp-app/README.md) file

Setting up poc-sso-sp-oauth2

Follow the instructions in the README.md file

Setting up poc-sso-sp-saml

Follow the instructions in the [README.md](./sso-springboot-poc/poc-sso-sp-saml/README.md) file


## Notes

**OAuth 2.0** - is not an authentication protocol but an open standard that provides authication via delegation, consent, and authorization

## References

[ldap Federation with keycloak](https://idaccessman.wordpress.com/2018/08/26/ldap-federation-with-keycloak/)

[csv2ldif2](https://sourceforge.net/projects/csv2ldif2/)

[RFC-6749](https://tools.ietf.org/html/rfc6749)

[RFC-7522: Oauth 2.0 - SAML](https://tools.ietf.org/html/rfc7522)

