
# quarkus-bindy-poc project

POC to convert multiple file types to a common csv file using Quarkus and apache camel.  

When starting the application, the following directories will be created or generated throughout the applications life time

	$BEGUI_FILE_ROOT/filetype1
	$BEGUI_FILE_ROOT/filetype2

There are two flat file types in the following resource directory

	resources/fs/filetype1/

	resources/fs/filetype2/
	
Drop the files in the respective directory to generate the output 


## Environment Variables

You can overwrite the 

    BEGUI_FILE_ROOT 

path to specify the file root, this value is currently set to 

    $HOME/tmp/quarkus-bindy-poc


## Known issues

* For some reason the application isn't reading environment variables I define in eclipse ide. 

* In eclipse, mapstruct processor seems to fail to generate the classes initially. I had to edit the interface to trigger the regeneration of the class implementation. There could be a property somewhere to fix this.. 


This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-bindy-poc-1.0.0-SNAPSHOT-runner.jar` file in the `/target` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/lib` directory.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-bindy-poc-1.0.0-SNAPSHOT-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/quarkus-bindy-poc-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.html.

# Guides: 

[Apache Camel Bindy](https://camel.apache.org/components/latest/dataformats/bindy-dataformat.html)

[Quarkus Rest Json](https://quarkus.io/guides/rest-json)

[Micrometer](https://quarkus.io/guides/micrometer)

[Quarkus Camel Testing](https://camel.apache.org/camel-quarkus/latest/user-guide/testing.html)
