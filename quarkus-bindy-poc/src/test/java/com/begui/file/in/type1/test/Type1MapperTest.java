package com.begui.file.in.type1.test;

import java.math.BigDecimal;
import java.text.ParseException;

import javax.inject.Inject;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.begui.file.in.type1.Record01;
import com.begui.file.in.type1.Record02;
import com.begui.file.in.type1.Type1Mapper;
import com.begui.file.out.filetype.Order;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class Type1MapperTest {

	@Inject
	Type1Mapper mapper;

	@Test
	public void mappingRecord01Test() throws ParseException {

		var record01 = new Record01();
		record01.setBatch("00001");
		record01.setRecordDate(DateUtils.parseDate("01/31/1980", new String[] { "MM/dd/yyyy" }));
		record01.setRecordType(1);

		Order outOrder = new Order();
		mapper.updateOutOrder(record01, outOrder);

		Assertions.assertEquals("00001", outOrder.getBatch());
		Assertions.assertNull(outOrder.getAmount());
		Assertions.assertNull(outOrder.getCurrency());
		Assertions.assertNull(outOrder.getFirstName());
		Assertions.assertNull(outOrder.getLastName());
		Assertions.assertNull(outOrder.getFromFile());
		Assertions.assertNull(outOrder.getOrderDate());
		Assertions.assertNull(outOrder.getOrderType());
	}

	@Test
	public void mappingRecord02Test() throws ParseException {
		
		final var orderDate = DateUtils.parseDate("01/31/1980", new String[] { "MM/dd/yyyy" });

		var record02 = new Record02();
		record02.setAmount(BigDecimal.TEN);
		record02.setCurrency("USD");
		record02.setFirstName("firstName");
		record02.setLastName("lastName");
		record02.setOrderDate(orderDate);
		record02.setOrderType("BUY");
		record02.setRecordNumber(2);

		var outOrder = mapper.toOutOrder(record02);
		Assertions.assertNotNull(outOrder);
		Assertions.assertNull(outOrder.getBatch());
		Assertions.assertEquals(BigDecimal.TEN, outOrder.getAmount());
		Assertions.assertEquals("USD", outOrder.getCurrency());
		Assertions.assertEquals("firstName", outOrder.getFirstName());
		Assertions.assertEquals("lastName", outOrder.getLastName());
		Assertions.assertEquals("Type1", outOrder.getFromFile());
		Assertions.assertEquals(orderDate, outOrder.getOrderDate());
		Assertions.assertEquals("BUY", outOrder.getOrderType());
	}

}
