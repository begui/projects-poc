package com.begui.file.in.type2.test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;

import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class Route2Test {

	@Inject
	CamelContext camelContext;

	@Inject
	ProducerTemplate producerTemplate;

	@Test
	void routeTest() throws IOException, InterruptedException {
		final String fileData = IOUtils.resourceToString("fs/filetype2/filetype2.txt", Charset.defaultCharset(),
				getClass().getClassLoader());
		MockEndpoint mockEndpoint = camelContext.getEndpoint(Mock2Route.END, MockEndpoint.class);
		mockEndpoint.setExpectedCount(1);
		producerTemplate.sendBody(Mock2Route.START, fileData);
		mockEndpoint.assertIsSatisfied();

		Exchange exchange = mockEndpoint.getExchanges().get(0);
		ArrayList<com.begui.file.out.filetype.Order> fromList = exchange.getIn().getBody(ArrayList.class);
		Assertions.assertEquals(4, fromList.size());

		{
			final var item = fromList.get(0);
			Assertions.assertNotNull(item);
			Assertions.assertEquals(BigDecimal.valueOf(2500.45), item.getAmount());
			Assertions.assertEquals("USD", item.getCurrency());
			Assertions.assertEquals("00001", item.getBatch());
			Assertions.assertEquals("FirstName1", item.getFirstName());
			Assertions.assertEquals("LastName1", item.getLastName());
			Assertions.assertEquals("Type2", item.getFromFile());
			Assertions.assertEquals("BUY", item.getOrderType());
		}

		{
			final var item = fromList.get(1);
			Assertions.assertNotNull(item);
			Assertions.assertEquals(BigDecimal.valueOf(1500.00).setScale(2), item.getAmount());
			Assertions.assertEquals("USD", item.getCurrency());
			Assertions.assertEquals("00001", item.getBatch());
			Assertions.assertEquals("FirstName1", item.getFirstName());
			Assertions.assertEquals("LastName1", item.getLastName());
			Assertions.assertEquals("Type2", item.getFromFile());
			Assertions.assertEquals("SELL", item.getOrderType());
		}
		{
			final var item = fromList.get(2);
			Assertions.assertNotNull(item);
			Assertions.assertEquals(BigDecimal.valueOf(1500.00).setScale(2), item.getAmount());
			Assertions.assertEquals("USD", item.getCurrency());
			Assertions.assertEquals("00001", item.getBatch());
			Assertions.assertEquals("FirstName2", item.getFirstName());
			Assertions.assertEquals("LastName2", item.getLastName());
			Assertions.assertEquals("Type2", item.getFromFile());
			Assertions.assertEquals("BUY", item.getOrderType());
		}
		{
			final var item = fromList.get(3);
			Assertions.assertNotNull(item);
			Assertions.assertEquals(BigDecimal.valueOf(100.00).setScale(2), item.getAmount());
			Assertions.assertEquals("USD", item.getCurrency());
			Assertions.assertEquals("00001", item.getBatch());
			Assertions.assertEquals("FirstName2", item.getFirstName());
			Assertions.assertEquals("LastName2", item.getLastName());
			Assertions.assertEquals("Type2", item.getFromFile());
			Assertions.assertEquals("BUY", item.getOrderType());
		}

	}

}
