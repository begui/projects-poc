package com.begui.file.in.type2.test;

import java.math.BigDecimal;
import java.text.ParseException;

import javax.inject.Inject;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.begui.file.in.type2.Record09;
import com.begui.file.in.type2.Record01;
import com.begui.file.in.type2.Record02;
import com.begui.file.in.type2.Record03;
import com.begui.file.in.type2.Type2Mapper;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class Type2MapperTest {

	@Inject
	Type2Mapper mapper;

	@Test
	public void mappingAllTest() throws ParseException {
		final var recordDate = DateUtils.parseDate("01/31/1980", new String[] { "MM/dd/yyyy" });

		Record01 record01 = new Record01();
		record01.setBatch("00001");
		record01.setRecordDate(recordDate);
		record01.setRecordType(1);

		Record02 record02 = new Record02();
		record02.setFirstName("firstName");
		record02.setLastName("lastName");
		record02.setOrderDate(recordDate);
		record02.setRecordNumber(2);

		Record09 record09 = new Record09();
		record09.setNumberOfRecordsInTheFile(1);
		record09.setRecordType(9);

		Record03 record03 = new Record03();
		record03.setAmount(BigDecimal.TEN);
		record03.setCurrency("USD");
		record03.setOrderType("BUY");
		record03.setRecordNumber(1);
		record03.setR1(record01);
		record03.setR2(record02);
		record03.setR9(record09);

		var outOrder = mapper.toOutOrder(record03);
		Assertions.assertNotNull(outOrder);
		Assertions.assertEquals("00001", outOrder.getBatch());
		Assertions.assertEquals(BigDecimal.TEN, outOrder.getAmount());
		Assertions.assertEquals("USD", outOrder.getCurrency());
		Assertions.assertEquals("firstName", outOrder.getFirstName());
		Assertions.assertEquals("lastName", outOrder.getLastName());
		Assertions.assertEquals("Type2", outOrder.getFromFile());
		Assertions.assertEquals(recordDate, outOrder.getOrderDate());
		Assertions.assertEquals("BUY", outOrder.getOrderType());

	}

}
