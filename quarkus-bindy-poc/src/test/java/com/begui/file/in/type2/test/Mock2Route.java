package com.begui.file.in.type2.test;

import javax.enterprise.context.ApplicationScoped;

import com.begui.file.in.type2.Route;

@ApplicationScoped
public class Mock2Route extends Route {

	public static final String START = "direct:MockRouteType2Start";
	public static final String END = "mock:MockRouteType2Result";

	@Override
	public void configure() throws Exception {
		this.route(from(START)).to(END);
	}
}
