package com.begui.file.in.type1.test;

import javax.enterprise.context.ApplicationScoped;

import com.begui.file.in.type1.Route;

@ApplicationScoped
public class Mock1Route extends Route {

	public static final String START = "direct:MockRouteType1Start";
	public static final String END = "mock:MockRouteType1Result";

	@Override
	public void configure() throws Exception {
		this.route(from(START)).to(END);
	}
}
