package com.begui.file.out.filetype;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.BindyType;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class Route extends RouteBuilder {

	@ConfigProperty(name = "application.file.root")
	String toPath;

	@Override
	public void configure() throws Exception {
		from("direct:handleOrders") //
				.marshal().bindy(BindyType.Csv, Order.class) //
				.to("file:" + toPath + "/out/?filename=${file:onlyname.noext}.csv") //

		;
	}
}