package com.begui.file.out.filetype;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

@CsvRecord(generateHeaderColumns = true, separator = ",")
public class Order {
	
	@DataField(pos = 1, columnName = "Batch")
	private String batch;

	@DataField(pos = 2, columnName = "Type")
	private String orderType;

	@DataField(pos = 3, pattern = "dd-MM-yyyy", columnName = "Date")
	private Date orderDate;

	@DataField(pos = 4)
	private String fromFile;

	@DataField(pos = 5)
	private String firstName;

	@DataField(pos = 6)
	private String lastName;

	@DataField(pos = 7, precision = 2, length = 12, paddingChar = '0')
	private BigDecimal amount;

	@DataField(pos = 8, length = 3)
	private String currency;
	
	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getFromFile() {
		return fromFile;
	}

	public void setFromFile(String fromFile) {
		this.fromFile = fromFile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Order [batch=" + batch + ", orderType=" + orderType + ", orderDate=" + orderDate + ", fromFile="
				+ fromFile + ", firstName=" + firstName + ", lastName=" + lastName + ", amount=" + amount
				+ ", currency=" + currency + "]";
	}
	
	

}
