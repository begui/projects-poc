package com.begui.file.in.type2;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MapperConfig;
import org.mapstruct.Mapping;

@MapperConfig
@Mapper(componentModel = "cdi")
public interface Type2Mapper {

	@Mapping(target = "batch", source = "r1.batch")
	@Mapping(target = "orderType", source = "orderType")
	@Mapping(target = "orderDate", source = "r2.orderDate")
	@Mapping(target = "fromFile", constant = "Type2")
	@Mapping(target = "firstName", source = "r2.firstName")
	@Mapping(target = "lastName", source = "r2.lastName")
	@Mapping(target = "amount", source = "amount")
	@Mapping(target = "currency", source = "currency")
	public com.begui.file.out.filetype.Order toOutOrder(Record03 order);

	public List<com.begui.file.out.filetype.Order> toOutOrderList(List<Record03> order);
}
