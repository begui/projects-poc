package com.begui.file.in.type2;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class Record02 {
	@DataField(pos = 22, length = 2, paddingChar = '0')
	private int recordNumber;

	@DataField(pos = 24, length = 10, pattern = "dd-MM-yyyy")
	private Date orderDate;

	@DataField(pos = 34, length = 15, align = "L", trim = true)
	private String firstName;

	@DataField(pos = 49, length = 15, align = "L", trim = true)
	private String lastName;

	public int getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Record02 [recordNumber=" + recordNumber + ", orderDate=" + orderDate + ", firstName=" + firstName
				+ ", lastName=" + lastName + "]";
	}

}
