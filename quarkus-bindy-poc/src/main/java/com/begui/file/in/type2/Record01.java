package com.begui.file.in.type2;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;

public class Record01 {
	@DataField(pos = 64, length = 2, paddingChar = '0')
    private int recordType ;
	
	@DataField(pos=66, length=5, paddingChar = '0')
	private String batch;

    @DataField(pos = 71, length = 10, pattern = "dd-MM-yyyy")
    private Date recordDate;

	public int getRecordType() {
		return recordType;
	}

	public void setRecordType(int recordType) {
		this.recordType = recordType;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	@Override
	public String toString() {
		return "Record01 [recordType=" + recordType + ", batch=" + batch + ", recordDate=" + recordDate + "]";
	}
    
    
}
