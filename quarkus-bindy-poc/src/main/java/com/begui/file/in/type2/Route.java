package com.begui.file.in.type2;

import java.util.ArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.fixed.BindyFixedLengthDataFormat;
import org.apache.camel.model.RouteDefinition;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class Route extends RouteBuilder {
	private static final Logger LOG = LoggerFactory.getLogger(Route.class);

	@ConfigProperty(name = "application.file.root")
	String fromPath;

	@Inject
	Type2Mapper mapper;

	@Override
	public void configure() throws Exception {
		var fileRoute = String.format("file:%s/filetype2/?autoCreate=true&move=archive&moveFailed=error", fromPath);
		LOG.info("Route on {}", fileRoute);
		this.route(from(fileRoute)) //
				.log("Body2:\n${body}") //
				.to("direct:handleOrders") //
				.end();
	}

	@SuppressWarnings("unchecked")
	protected RouteDefinition route(RouteDefinition route) {
		return route.bean(Bean.class) //
				.log("Body1:\n${body}") //
				.unmarshal(new BindyFixedLengthDataFormat(Record03.class)) //
				.log("Body2:\n${body}") //
				.process(exchange -> {
					// Get Body
					ArrayList<Record03> fromList = exchange.getIn().getBody(ArrayList.class);
					print(fromList);
					var toList = mapper.toOutOrderList(fromList);
					exchange.getIn().setBody(toList);
				});
	}

	private void print(ArrayList<Record03> body) {
		//
		for (var order : body) {
			LOG.info("{}", order);
		}
		LOG.info("Total records received by the bean : {}" , body.size());

	}

}
