package com.begui.file.in.type2;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.apache.camel.Handler;

public class Bean {

	/**
	 * This code assumes the file is formatted correctly. It will treat 03 records
	 * as the 'master' or 'primary' record and append the following 01 (header),
	 * 09(footer) and 02 (detail) records into one string. Once we do this we can
	 * use Bindy to easily convert
	 * 
	 * TOD
	 * 
	 * @Handler This is probably not needed in this case however Bean Binding in
	 *          Camel defines both which methods are invoked and also how the
	 *          Message is converted into the parameters of the method when it is
	 *          invoke
	 * 
	 * @param custom This is the entire payload of the file in this string. There
	 *               should be a better way to handle this
	 * @return the custom string we will generate. We will append header information
	 *         each row of the
	 */
	@Handler
	public String handle(String custom) {

		var sb = new StringBuilder();
		final String lines[] = custom.split("\\r?\\n");

		String record01 = null;
		String record02 = null;

		var record03List = new ArrayList<String>();
		for (var line : lines) {
			if (line.startsWith( "01")) {
				record01 = line;
			} else if (line.startsWith("02")) {
				record02 = line;
			} else if (line.startsWith( "03")) {
				record03List.add(new StringBuilder().append(line).append(record02).toString());
			} else if (line.startsWith("09")) {
				// This is the 09 footer record or end record. Lets append and clean out
				sb.append(formatRecord(record03List, record01, line));
				record03List.clear();
				record02 = null;
				record01 = null;
			}

		}

		return sb.toString();
	}

	private String formatRecord(ArrayList<String> r3List, final String r1, final String r9) {
		r3List.replaceAll(s -> new StringBuilder().append(s).append(r1).append(r9).toString().trim());
		return r3List.stream().collect(Collectors.joining(System.lineSeparator()));
	}

}
