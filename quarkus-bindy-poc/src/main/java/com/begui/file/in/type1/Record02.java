package com.begui.file.in.type1;

import java.math.BigDecimal;
import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord(header = Record01.class, footer = Record09.class)
public class Record02 {
	@DataField(pos = 1, length = 2)
	private int recordNumber;
	
	@DataField(pos = 3, length = 10, pattern = "dd-MM-yyyy")
	private Date orderDate;

	@DataField(pos = 13, length = 15, align = "L", trim = true)
	private String firstName;

	@DataField(pos = 28, length = 15, align = "L", trim = true)
	private String lastName;

	@DataField(pos = 43, length = 4, trim = true)
	private String orderType;

	@DataField(pos = 47, precision = 2, length = 12, paddingChar = '0')
	private BigDecimal amount;

	@DataField(pos = 59, length = 3)
	private String currency;

	public int getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Order [recordNumber=" + recordNumber + ", orderDate=" + orderDate + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", orderType=" + orderType + ", amount=" + amount + ", currency="
				+ currency + "]";
	}

}
