package com.begui.file.in.type2;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class Record09 {
	@DataField(pos = 81, length = 2)
	private Integer recordType;

	@DataField(pos = 83, length = 9)
	private int numberOfRecordsInTheFile;

	public Integer getRecordType() {
		return recordType;
	}

	public void setRecordType(Integer recordType) {
		this.recordType = recordType;
	}

	public int getNumberOfRecordsInTheFile() {
		return numberOfRecordsInTheFile;
	}

	public void setNumberOfRecordsInTheFile(int numberOfRecordsInTheFile) {
		this.numberOfRecordsInTheFile = numberOfRecordsInTheFile;
	}

	@Override
	public String toString() {
		return "Record09 [recordType=" + recordType + ", numberOfRecordsInTheFile=" + numberOfRecordsInTheFile + "]";
	}
	
	
}
