package com.begui.file.in.type1;

import java.util.ArrayList;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.fixed.BindyFixedLengthDataFormat;
import org.apache.camel.model.RouteDefinition;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class Route extends RouteBuilder {
	private static final Logger LOG = LoggerFactory.getLogger(Route.class);

	@ConfigProperty(name = "application.file.root")
	String fromPath;

	@Inject
	Type1Mapper mapper;

	@Override
	public void configure() throws Exception {
		var fileRoute = String.format("file:%s/filetype1/?autoCreate=true&move=archive&moveFailed=error", fromPath);
		LOG.info("Route on {}", fileRoute);
		this.route(from(fileRoute)) //
				.log("${body}") //
				.to("direct:handleOrders") //
				.end();
	}

	@SuppressWarnings("unchecked")
	protected RouteDefinition route(RouteDefinition route) {

		return route.unmarshal(new BindyFixedLengthDataFormat(Record02.class)) //
				.process(exchange -> {
					Map<String, Record01> headerMap = exchange.getIn()
							.getHeader(BindyFixedLengthDataFormat.CAMEL_BINDY_FIXED_LENGTH_HEADER, Map.class);
					Record01 header = headerMap.get(Record01.class.getName());
					// Get Footer
					Map<String, Record09> footerMap = (Map<String, Record09>) exchange.getIn()
							.getHeader(BindyFixedLengthDataFormat.CAMEL_BINDY_FIXED_LENGTH_FOOTER, Map.class);
					Record09 footer = footerMap.get(Record09.class.getName());
					// Get Body
					ArrayList<Record02> fromList = exchange.getIn().getBody(ArrayList.class);
					print(header, footer, fromList);
					// Now transform the data
					var toList = mapper.toOutOrder(fromList);
					toList.stream().forEach(order -> mapper.updateOutOrder(header, order));
					// TODO: fix me
//					mapper.updateOutOrder(header, toList);
					exchange.getIn().setBody(toList);
				});

	}

	private void print(final Record01 header, final Record09 footer, final ArrayList<Record02> body) {
		LOG.info("Header Record type: {}", header.getRecordType());
		LOG.info("...date1: {}", header.getRecordDate());

		LOG.info("Footer Record type: {}", footer.getRecordType());
		LOG.info("... NumOfRecs : {}", footer.getNumberOfRecordsInTheFile());
		//
		for (var order : body) {
			LOG.info("{}", order);
		}
		LOG.info("Total records received by the bean : " + body.size());

	}

}
