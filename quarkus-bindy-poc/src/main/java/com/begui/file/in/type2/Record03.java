package com.begui.file.in.type2;

import java.math.BigDecimal;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;
import org.apache.camel.dataformat.bindy.annotation.Link;

@FixedLengthRecord
public class Record03 {
	@DataField(pos = 1, length = 2)
	private int recordNumber;
	
	@DataField(pos = 3, length = 4, trim = true)
	private String orderType;

	@DataField(pos = 7, precision = 2, length = 12, paddingChar = '0')
	private BigDecimal amount;

	@DataField(pos = 19, length = 3)
	private String currency;
	
	@Link
	private Record02 r2;
	
	@Link
	private Record01 r1;

	@Link
	private Record09 r9;

	public int getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Record02 getR2() {
		return r2;
	}

	public void setR2(Record02 r2) {
		this.r2 = r2;
	}

	public Record01 getR1() {
		return r1;
	}

	public void setR1(Record01 r1) {
		this.r1 = r1;
	}
	

	public Record09 getR9() {
		return r9;
	}

	public void setR9(Record09 r9) {
		this.r9 = r9;
	}

	@Override
	public String toString() {
		return "Record03 [recordNumber=" + recordNumber + ", orderType=" + orderType + ", amount=" + amount
				+ ", currency=" + currency + ", r2=" + r2 + ", r1=" + r1 + ", r9=" + r9 + "]";
	}

	
	
	

}
