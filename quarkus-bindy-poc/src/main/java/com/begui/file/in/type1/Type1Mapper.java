package com.begui.file.in.type1;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "cdi")
public interface Type1Mapper {


//	@Mapping(target = "fromFile", constant = "Type1")
//	@Mapping(target = "batch", ignore = true)
//	public void toOutOrder(OrderHeader header, @MappingTarget com.begui.file.out.filetype.Order order);

	@Mapping(target = "fromFile", constant = "Type1")
	@Mapping(target = "batch", ignore = true)
	public com.begui.file.out.filetype.Order toOutOrder(Record02 order);
	public List<com.begui.file.out.filetype.Order> toOutOrder(List<Record02> order);


	@Mapping(target = "batch", source = "batch")
	@Mapping(target = "orderType", ignore = true)
	@Mapping(target = "orderDate", ignore = true)
	@Mapping(target = "fromFile", ignore = true)
	@Mapping(target = "firstName", ignore = true)
	@Mapping(target = "lastName", ignore = true)
	@Mapping(target = "amount", ignore = true)
	@Mapping(target = "currency", ignore = true)
	public void updateOutOrder(Record01 header, @MappingTarget com.begui.file.out.filetype.Order order);
	// TODO: Look into why this isn't generating
//	public void updateOutOrder(OrderHeader header, @MappingTarget List<com.begui.file.out.filetype.Order> order);

}