package com.begui.file.in.type1;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class Record09 {
	@DataField(pos = 1, length = 2)
	private int recordType = 9;

	@DataField(pos = 3, length = 9, align = "R", paddingChar = '0')
	private int numberOfRecordsInTheFile;

	public int getRecordType() {
		return recordType;
	}

	public void setRecordType(int recordType) {
		this.recordType = recordType;
	}

	public int getNumberOfRecordsInTheFile() {
		return numberOfRecordsInTheFile;
	}

	public void setNumberOfRecordsInTheFile(int numberOfRecordsInTheFile) {
		this.numberOfRecordsInTheFile = numberOfRecordsInTheFile;
	}
	
	
}
