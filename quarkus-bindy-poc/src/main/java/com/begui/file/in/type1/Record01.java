package com.begui.file.in.type1;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.DataField;
import org.apache.camel.dataformat.bindy.annotation.FixedLengthRecord;

@FixedLengthRecord()
public class Record01 {
	@DataField(pos = 1, length = 2)
	private int recordType = 1;

	@DataField(pos = 3, length = 10, pattern = "dd-MM-yyyy")
	private Date recordDate;

	@DataField(pos = 13, length = 5)
	private String batch;

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public int getRecordType() {
		return recordType;
	}

	public void setRecordType(int recordType) {
		this.recordType = recordType;
	}

	public Date getRecordDate() {
		return recordDate;
	}

	public void setRecordDate(Date recordDate) {
		this.recordDate = recordDate;
	}

	@Override
	public String toString() {
		return "OrderHeader [recordType=" + recordType + ", recordDate=" + recordDate + ", batch=" + batch + "]";
	}

}
